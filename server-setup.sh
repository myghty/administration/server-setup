#!/bin/bash

# MyGHTY Server Setup
# inspired by this webpage : https://clouding.io/hc/en-us/articles/360010612920-How-to-Deploy-Symfony-with-Nginx-on-Ubuntu-18-04

# APT Repositories Update
aptInstall()
{
    apt update;
    apt upgrade;

    apt-get install nginx -y;
    apt-get install mariadb-server -y;
    apt-get install php php-fpm php-mbstring php-opcache php-xml php-zip php-curl php-mysql php-cli php-intl -y;
    apt-get install nodejs npm -y;
    apt-get install curl -y;
    apt-get install git -y;
}

# PHP FPM Configuration
phpFpmConfigEdit()
{
    sed -i 's/memory_limit = 128M/memory_limit = 256M/g' /etc/php/7.2/fpm/php.ini;
    sed -i 's/;cgi.fix_pathinfo=1/cgi.fix_pathinfo = 0/g' /etc/php/7.2/fpm/php.ini;
    sed -i 's/max_execution_time = 30/max_execution_time = 129/g' /etc/php/7.2/fpm/php.ini;
    sed -i 's/max_input_time = 60/max_input_time = 300/g' /etc/php/7.2/fpm/php.ini;
    sed -i "s/;date.timezone =/date.timezone = 'UTC'/g" /etc/php/7.2/fpm/php.ini;
    echo "safe_mode = Off" >> /etc/php/7.2/fpm/php.ini;
}

# Composer Installation
composerInstall()
{
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer;
}

# Symfony Cli Installation
symfonyInstall()
{
    wget https://get.symfony.com/cli/installer -O - | bash;
    mv /root/.symfony/bin/symfony /usr/local/bin/symfony;
}

nodeJsConfigEdit()
{
    ln -s /usr/bin/nodejs /usr/local/bin/node;
    ln -s /usr/bin/npm /usr/local/bin/npm;
    npm install -g supervisor;
}

# Get templates
getTemplates()
{
    wget -P /root/ https://gitlab.com/myghty/administration/server-setup/-/raw/master/nginx-site-available-template
    wget -P /root/ https://gitlab.com/myghty/administration/server-setup/-/raw/master/create-new-sf-site.sh
}

aptInstall;
phpFpmConfigEdit;
composerInstall;
symfonyInstall;
getTemplates;
